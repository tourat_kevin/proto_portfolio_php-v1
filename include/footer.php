       <script src="https://formspree.io/js/formbutton-v1.0.0.min.js" defer></script>
        <script>
            /* paste this in verbatim */
            window.formbutton=window.formbutton||function(){(formbutton.q=formbutton.q||[]).push(arguments);};
            /* customize formbutton here*/     
            formbutton("create", {
            action: "https://formspree.io/f/xzbkdvoo",
            buttonImg: "<i class='fas fa-mail-bulk' style='font-size: 32px'/>", // <-- new button icon
            title: "Envie de me contacter ?",
            fields: [
                { 
                type: "email", 
                label: "Email:", 
                name: "email",
                required: true,
                placeholder: "votre@email.com"
                },
                { 
                type: "text", 
                label: "Nom:", 
                name: "nom",
                required: true,
                placeholder: "Votre nom"
                },
                {
                type: "textarea",
                label: "Message:",
                name: "message",
                placeholder: "Votre message",
                },
                { type: "submit" }      
            ],
            styles: {
                fontFamily: "Montserrat",
                title: {
                background: "#2c61e3",
                },
                button: {
                background: "#2c61e3",
                }
            },
            initiallyVisible: false,
            });
        </script>
        <div class="space"></div>
    <footer>
    <!-- <div class="space-test"></div> -->
    <div class="container-fluid hero__content footer" id="contact">
            <div class="row">      
                <div class="col-md-4 mt-5 footer">
                    <p>Mail: <a href="mailto:tourat.kevin@gmail.com">tourat.kevin@gmail.com</a></p>
                    <p>Tel: <a href="tel:0671041464">06.71.04.14.64</a></p>
                </div>
                <div class="col-md-4">
                    <div class="footer">
                        <div class="social">
                            <div class="space-30"></div>
                            <a href="https://gitlab.com/tourat_kevin" alt="GitLab" target="_blank" rel="noopener" title="GitLab"><i class="fab fa-gitlab"></i></a>
                            <a class="in" href="https://www.linkedin.com/in/k%C3%A9vin-tourat-8b0657195/" target="_blank" rel="noopener" title="Linkedin"><i class="fab fa-linkedin"></i></a>
                            <a href="https://twitter.com/TouratKevin" target="_blank" rel="noopener"><i class="fab fa-twitter" title="Twitter"></i></a>
                        </div>
                        <div class="space-50"></div> 
                        <p>&copy; Tourat Kévin 2020</p>                    
                    </div>
                </div>
                <div class="col-md-4 mt-5">  
                    <p>Confidentialité</p><a href="https://formspree.io/legal/privacy-policy">RGPD Formspree</a>
                    <div class="space"></div>
                </div>
            </div>
        </div>
    <!-- Cookie --><!-- bannière cookie -->
    <div class="btncookie" id="boxcookie">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <p class="p-cookie">Nous utilisons des cookies pour vous garantir une meilleure expérience et améliorer la performance de notre site.</p>           
                </div>
                <div class="col-md-4">
                    <button id="nocookie" type="button" class="btn btn-primary">Non</button>
                    <button id="okcookie" type="button" class="btn btn-primary">Oui</button>
                </div>
            </div>
        </div>
         
    </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.4/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-beta1/js/bootstrap.min.js"></script> -->
    <script src='assets/js/cookie.js'></script>
	<script src="dist/js/lightbox-plus-jquery.js"></script>

</body>
</html>