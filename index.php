<?php require_once('include/header.php');?>


    <!-- navbar here -->
    <?php include('include/navbar.php');?>
 
  <!-- Main -->
  <main class="main main-raised">

    <section class="bg-main no-vh" id="about">
      <div class="space-50 space-mobile"></div>
      <h2 class="my-4">About</h2>
      <h2 class="h6">Qui suis-je ?</h3>
      <div class="space-50"></div>
      <div class="container bg-about">

        <!-- static -->
        <div class="row">
          <div class="col-md-3 text-left text-center-item photo-about">
            <img class="me" src="resources/image/Ellipse-me.png" alt="">
          </div>
          <div class="col-md-9 text-justify-about">
            <p>Né dans le sud de la France, Sète (34) - 1989. Je suis un développeur français avec l'envie de me spécialiser dans le design numérique. Je centre mon travail sur le maintien d'un design et d'un code précis, propre et élégant sur les sites Web ou les applications que je crée. Mes expériences lors de ma formation m'ont appris l'importance de l'expérience utilisateur et m'ont apporté l'amour du pixel parfait. J'ai également eu la chance de travailler avec des personnes passionnées sur de beaux projets pour des clients.</p>           
          </div>
        </div>
        <!-- fin static -->

      </div>
    </section>

    <div class="space"></div>
    
      <section class="bg-main  no-vh" id="skills">      
        <div class="space-50 space-mobile"></div>
        <h2 class="my-4">Skills</h2>
        <h2 class="h6">Compétences</h2>
        <h2 class="h6">Technologies & Softwares</h2>
        <div class="space-25"></div>
        
            <?php
              require 'src/database.php';
          
              // ici on viens faire la premiere requete orm pour afficher les 4 premières image de la table img_technologies 
              // dans la col-md-6 puis la seconde req orm on vien afficher les 4 dernieres image de la table.
              $reqSkill = ('SELECT * FROM img_technologies ORDER BY `id` DESC LIMIT 4');
              $reqSkill = $db->prepare($reqSkill);
              $reqSkill->execute();

              $reqSkill2 = ('SELECT * FROM img_technologies ORDER BY `id` LIMIT 4');
              $reqSkill2 = $db->prepare($reqSkill2);
              $reqSkill2->execute();

              // même opération avec la table img_softwares
              $reqSkill3 = ('SELECT * FROM img_softwares ORDER BY `id` DESC LIMIT 4');
              $reqSkill3 = $db->prepare($reqSkill3);
              $reqSkill3->execute();

              $reqSkill4 = ('SELECT * FROM img_softwares ORDER BY `id` LIMIT 4');
              $reqSkill4 = $db->prepare($reqSkill4);
              $reqSkill4->execute();

              echo '<div class="container text-center-item" style="padding: 50px !important">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="txt-entete">
                            <section>';
                
                              while($colSkill1 = $reqSkill->fetch()){        
                                echo '<img class="img-skills" rel="tooltip" title="'.$colSkill1['title_tooltip_tech'].'" src="'.$colSkill1['url_img_tech'].'"  alt="'.$colSkill1['alt_img_tech'].'">';
                              }
                              echo '<br>';
                              while($colSkill2 = $reqSkill2->fetch()){        
                                echo '<img class="img-skills" rel="tooltip" title="'.$colSkill2['title_tooltip_tech'].'" src="'.$colSkill2['url_img_tech'].'"  alt="'.$colSkill2['alt_img_tech'].'">';
                              }
                              echo '</section>
                                  
                              </div>
                            </div>';

              echo '<div class="col-md-6">
                  <div class="txt-entete">
                    <section>';
        
                    while($colSkill3 = $reqSkill3->fetch()){        
                      echo '<img class="img-skills" rel="tooltip" title="'.$colSkill3['title_tooltip_soft'].'" src="'.$colSkill3['url_img_soft'].'"  alt="'.$colSkill3['alt_img_soft'].'">';
                    }
                    echo '<br>';
                    while($colSkill4 = $reqSkill4->fetch()){        
                      echo '<img class="img-skills" rel="tooltip" title="'.$colSkill4['title_tooltip_soft'].'" src="'.$colSkill4['url_img_soft'].'"  alt="'.$colSkill4['alt_img_soft'].'">';
                    }
                      echo '</section>
                          </div>
                        </div>
                      </div>
                    </div>';
                // Database::disconnect();
            ?>
            
        <div class="container text-center-item" style="padding: 50px !important">
          <div class="row">
            <div class="col-md-12">
              <div class="txt-entete">
                  <div class="space-30"></div>
                  <section>                                                                               
                      <img src="resources/image/picto-skills/laragon_logo1.png" class="img-skill" alt="Logo compétences js">
                      <img src="resources/image/picto-skills/500px-GitLab_logo-100.png" class="img-skill" alt="Logo compétences">
                      <img src="resources/image/picto-skills/postman.png" class="img-skill" alt="Logo compétences js">
                  </section>
              </div>
            </div>
          </div>
        </div>
    </section>

    

    

    <div class="container">
    <section class="bg-main section-timeline no-vh" id="experience">
      <div class="space-70 space-mobile"></div>
      <h2  class="mt-4">Experience </h2>
      <h2 class="h6">&<br>Formation</h2>
      <div class="space-70"></div>
        <div class="container">
          <div class="row">

            <?php
              require 'src/database.php';

            
              // $db = Database::connect();
              $reqComp = 'SELECT * FROM `timeline` ORDER BY `year` DESC';
              $reqComp = $db->prepare($reqComp);
              $reqComp->execute();
              
              // var_dump($reqComp);

              // Database::disconnect();
              while ($donnees = $reqComp->fetch()) {
                echo '<div class="col-md-12">
                <div class="main-timeline">
                  <div class="timeline">
                    <div class="timeline-icon"><span class="year">'.$donnees['year'].'</span></div>
                      <div class="timeline-content">
                        <h2 class="title">'.$donnees['title'].'</h2>
                        <img class="rounded" src="'.$donnees['image'].'" alt="imts logo">
                        <h4 class="title">'.$donnees['structure'].'</h4>
                        <p class="description">
                        '.$donnees['content'].'
                        </p>
                    </div>
                  </div>
                </div>
              </div>
  
              ';
            
              }
            ?>
            
          </div>
        </div>
    </section>
    </div>

    <div id="projects" class="space"></div>
    <section class="no-vh" >
    
      <div class="container text-center-item">
        <h2 class="my-4">Projects</h2>
        <div class="space-50 space-mobile"></div>
        <div class="row">

            <?php
            
                require 'src/database.php';
                // $db = Database::connect();

                  $requete1 = $db->query('SELECT * FROM img_gaiago LIMIT 3 OFFSET 3' );
                  $gaiago_img = "resources/image/projet-gaiago.jpg";
                  $urlGalerie_gaiago = "gaiago/img-test.jpg";
                  $alt_img = "image du projet ";

                  echo '<div class="col-lg-4">
                          <a href="'.$urlGalerie_gaiago.'" data-alt="plage1" data-lightbox="gaiago-projet">
                            <img rel="tooltip" title="Projet Gaiago" class="hover-img" src="'.$gaiago_img.'" alt="'.$alt_img.'handisolidarites">
                          </a>';
              
                  while($donnees = $requete1->fetch()){
                  // $donnees devient un lien hypertext pour la galerie de projet. 
                  echo '' . $donnees['image'] . '';

                  }
                  
                  echo '</div>';

                  // 2

                  $requete1 = $db->query('SELECT * FROM img_alunissons' );
                  $alunissons_img = "resources/image/projet-alunissons.jpg";
                  $urlGalerie_alunissons = "alunissons/img-test.jpg";
                  $alt_img = "image du projet ";

                  echo '<div class="col-lg-4">
                          <a href="'.$urlGalerie_alunissons.'" data-alt="plage1" data-lightbox="alunissons-projet">
                            <img class="hover-img" src="'.$alunissons_img.'" alt="'.$alt_img.'handisolidarites">
                          </a>';
              
                  while($donnees = $requete1->fetch()){
                  // $donnees devient un lien hypertext pour la galerie de projet. 
                  echo '' . $donnees['image'] . '';
                          
                  }
                  
                  echo '</div>';

                  // 3

                  $requete1 = $db->query('SELECT * FROM img_handisolidarites' );
                  $handi_img = "resources/image/projet-handisolidarites.jpg";
                  $urlGalerie_handi = "handi/img-test.jpg";
                  $alt_img = "image du projet ";

                  echo '<div class="col-lg-4">
                          <a href="'.$urlGalerie_handi.'" data-alt="plage1" data-lightbox="handi-projet">
                            <img class="hover-img" src="'.$handi_img.'" alt="'.$alt_img.'handisolidarites">
                          </a>';
              
                  while($donnees = $requete1->fetch()){
                  // $donnees devient un lien hypertext pour la galerie de projet. 
                  echo '' . $donnees['image'] . '';
                          
                  }
                  
                  echo '</div>';
                  // Database::disconnect();

            ?>
          <div class="space-50"></div>
          <!-- projet 2 -->
          <div class="col-lg-12 mt-4 mb-4">
              <a href="https://gitlab.com/tourat_kevin" target="_blank" rel="noopener">
                  <img class="card" src="resources/image/gitLab_logo.png" alt="projet-git">
              </a>
            </div>
        </div>
      </div>
      <div class="container text-justify bg-1a46bfd3">
        <div class="row">
            <div class="col-lg-12">
              <h4 class="my-4">Recommandation</h4>
              <p><q> Je soussignée Marie-France BROCHET, en qualité de Présidente du Chœur ALUNISSONS de St Lunaire, atteste par la présente avoir confié la création du site internet de l'association "Chœur ALUNISSONS" à Monsieur Kevin TOURAT, dans le cadre de sa formation de développeur web et web mobile, à l'institut SOLACROUP à Dinard, au cours de l'année 2019-2020. Au cours de son travail, Kevin s'est intégré à une équipe de trois stagiaires et a su faire preuve d'initiative et de créativité avec sérieux et efficacité. Je le recommande sans hésitation. </q></p> 
             <p class="text-center my-4">Marie-France BROCHET, présidente.</p>
            </div>
        </div>
          <div class="space-50"></div>
 
        </div>

        
      
        
           
                 
      
    </section>

  </main>
  <div class="space"></div>
  <?php include('include/footer.php');?>